package model.data_structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;




public class ListaEncadenada<T> implements ILista<T> {
	
	private NodoSencillo<T> primero;
	private NodoSencillo<T> nodoActual;
	private NodoSencillo<T>	ultimo;

	@Override
	public Iterator<T> iterator() {
		MiIterador<T> it = new MiIterador<T>(primero);
		return it;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoSencillo<T> nodoAgregar = new NodoSencillo<T>(elem);
		if(primero == null){
			primero = nodoAgregar;
			nodoActual = primero;
			ultimo = primero;
		}

		else{
			ultimo.next = nodoAgregar;
			ultimo = nodoAgregar;

		}
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		NodoSencillo<T> actual = primero;
		while(actual != null && pos > 0)
		{
			actual = actual.next;
			pos--;
		}
		if(pos > 0)
			throw new IndexOutOfBoundsException();
		else
			return actual.element;
	}


	@Override
	public int darNumeroElementos() {
		int size = 0;
		NodoSencillo<T> actual = primero;
		while(actual != null){
			actual = actual.next;
			size++;
		}

		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return nodoActual.element;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		boolean avanzarSiguiente = false;
		if(nodoActual != ultimo){
			nodoActual = nodoActual.next;
			avanzarSiguiente = true;
		}

		return avanzarSiguiente;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		boolean avanzarSiguiente = false;
		
		if(nodoActual!=primero){
			NodoSencillo<T> anterior = primero;
		while (anterior.next != null && anterior.next != nodoActual){
			anterior = anterior.next;
		}
		nodoActual = anterior;
		avanzarSiguiente = true;
		}
		
		
		return avanzarSiguiente;
	}
	
	public void intercambiarElementosEn(int indA, int indB) {
		// TODO Auto-generated method stub
		NodoSencillo<T> nodoA = primero;
		NodoSencillo<T> nodoB = primero;
		
		while(indA>0 || indB>0){
			if(indA>0){
				nodoA=nodoA.next;
				indA--;
			}
			if(indB>0){
			nodoB=nodoB.next;
			indB--;
			}
		}
		
		T aux = nodoB.element;
		nodoB.element=nodoA.element;
		nodoA.element=aux;
		
	}

	private class NodoSencillo<T> {

		private T element;
		private NodoSencillo<T> next;

		public NodoSencillo (T pElement){
			next = null;
			element = pElement;
		}

		public T getElement(){
			return element;
		}

		public NodoSencillo<T> getNext(){
			return next;
		}

		public void setNext(NodoSencillo<T> pNext){
			next = pNext;
		}



	}
	
	private class MiIterador<T> implements Iterator<T>{

		NodoSencillo<T> actualIt;

		public MiIterador(NodoSencillo<T> pPrimero){
			actualIt = pPrimero;
		}

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actualIt != null;
		}
		@Override
		public T next() {
			T element = actualIt.getElement();
			actualIt = actualIt.next;
			return element;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}

	}

	public void shuffle() {
		// TODO Auto-generated method stub
		ArrayList<T> auxList = new ArrayList<>();
		for(T element: this)
			auxList.add(element);		

		Collections.shuffle(auxList);
		
		primero=ultimo=nodoActual=null;
		nodoActual=primero;
		for(T element: auxList){
			agregarElementoFinal(element);
		}
	}

}
