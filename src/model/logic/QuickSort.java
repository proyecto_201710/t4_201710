package model.logic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import model.data_structures.ListaEncadenada;
import model.vo.VOPelicula;


public class QuickSort <T extends Comparable<T>> {

	private BigDecimal tiempo;
	
	public void sort (T[] arreglo){
		
		ArrayList<T> auxList = (new ArrayList<>(Arrays.asList(arreglo)));
		Collections.shuffle(auxList);
		arreglo = auxList.toArray(arreglo);
		iniciarCronometro();
		sortAux(0, arreglo.length-1, arreglo);
		pararCronometro();
	}
	
	private void sortAux(int izq, int der, T[] arreglo){
		int pivot = izq;
		int inicial = izq;
		int fin = der;

		for(; izq <= der; ){
			//Si es menor que el pivote, intercambia el izquierdo con el pivote
			if(arreglo[izq].compareTo(arreglo[pivot]) < 0){
				intercambiar(izq, pivot, arreglo);
				pivot++;
				izq++;
			}
			//Si el pivote es igual al izquierdo avanza el indice del izquierdo
			else if(arreglo[izq].compareTo(arreglo[pivot]) == 0){
				izq++;

			}
			//Si es mayor, se avanza con el indice del derecho.
			else{

				for(boolean seEncontroUnMenor = false;!seEncontroUnMenor && der>= izq; der--){
					
					//En caso contrario, se intercambia el derecho con el izquierdo
					if(arreglo[der].compareTo(arreglo[pivot]) < 0){
						seEncontroUnMenor = true;	
						intercambiar(izq, der, arreglo);
						
					}
				}
			}
		}


	


	if(inicial<pivot-1)
		sortAux(inicial, pivot-1, arreglo);


	if(der<fin)
		sortAux(izq, fin, arreglo);

	//Por medio de recursividad de recorren las mitades del arreglo para ordenarlo





}

private void intercambiar(int indA, int indB,T[] arreglo){
	T aux = arreglo[indA];
	arreglo[indA] = arreglo[indB];
	arreglo[indB] = aux;
}

public void sort (ListaEncadenada<T> lista){
	
	lista.shuffle();
	iniciarCronometro();
	sortAux(0, lista.darNumeroElementos()-1, lista);
	pararCronometro();
}

private void sortAux(int izq, int der, ListaEncadenada<T> lista){
	int pivot = izq;
	int inicial = izq;
	int fin = der;

	for(; izq <= der; ){
		//Si es menor que el pivote, intercambia el izquierdo con el pivote
		T izquierdo = lista.darElemento(izq);
		T pivote = lista.darElemento(pivot);
		if(izquierdo.compareTo(pivote) < 0){
			intercambiar(izq, pivot, lista);
			pivot++;
			izq++;
		}
		//Si el pivote es igual al izquierdo avanza el indice del izquierdo
		else if(izquierdo.compareTo(pivote) == 0){
			izq++;

		}
		//Si es mayor, se avanza con el indice del derecho.
		else{

			T derecho = lista.darElemento(der);
			
			for(boolean seEncontroUnMenor = false;!seEncontroUnMenor && der>= izq; der--){
				derecho = lista.darElemento(der);
				//En caso contrario, se intercambia el derecho con el izquierdo
				if(derecho.compareTo(pivote) < 0){
					seEncontroUnMenor = true;	
					intercambiar(izq, der, lista);
					
				}
			}
		}
	}

if(inicial<pivot-1)
	sortAux(inicial, pivot-1, lista);


if(der<fin)
	sortAux(izq, fin, lista);

//Por medio de recursividad de recorren las mitades del arreglo para ordenarlo





}

private void intercambiar(int indA, int indB,ListaEncadenada<T> lista){
	lista.intercambiarElementosEn(indA, indB);
}






// -------------------------------------------------------
// Cronometro
// -------------------------------------------------------

public static void main(String[] args) {
	QuickSort<Integer> ordenadorNumeros = new QuickSort<>();
	Random r = new Random();
	Integer[] numeros = new Integer[10000000];
	Integer[] numeros2 = new Integer[10000000];
	for(int i = 0; i < numeros.length; i++){
		numeros[i]= numeros2[i] = r.nextInt(1000000000);
	}

	ordenadorNumeros.sort(numeros);
	System.out.println("QuickSort: " + ordenadorNumeros.darTiempo().doubleValue()/1000);
	
	BigDecimal time  = new BigDecimal(-System.currentTimeMillis());
	Arrays.sort(numeros2);
	BigDecimal fin = time.add(new BigDecimal(System.currentTimeMillis()));
	System.out.print("SortJava: " + fin.doubleValue()/1000 );
	
}

private void iniciarCronometro(){
	tiempo = new BigDecimal(-System.currentTimeMillis() + "");
}

private void pararCronometro(){
	tiempo = tiempo.add(new BigDecimal(System.currentTimeMillis() + ""));
}

public BigDecimal darTiempo(){
	
	return tiempo;
}



}
