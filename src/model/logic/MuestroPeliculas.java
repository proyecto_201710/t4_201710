package model.logic;

import java.io.*;
import java.util.Arrays;
import java.util.Random;
import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.vo.VOPelicula;
import java.util.Random;

public class MuestroPeliculas {

	private VOPelicula[] peliculas;

	public MuestroPeliculas(){
		cargarArchivoPeliculas("data/movies.csv");
	}
	public void cargarArchivoPeliculas(String archivoPeliculas){
		try{
			File archivoCSV = new File(archivoPeliculas);
			BufferedReader lector = new BufferedReader(new FileReader(archivoCSV));

			String linea = lector.readLine();
			linea = lector.readLine();

			ListaEncadenada<VOPelicula> lPeliculas = new ListaEncadenada<VOPelicula>();

			while(linea != null){
				// Recosntrucci�n del titulo de la pelicula

				String [] info = linea.split(",");
				String auxTitulo = "";


				for(int i = 1; i < info.length -1; i++)
					auxTitulo+= info[i] + ",";

				auxTitulo = auxTitulo.substring(0, auxTitulo.length()-1);
				if(auxTitulo.charAt(0) == '"')
					auxTitulo = auxTitulo.substring(1, auxTitulo.length() - 1);

				auxTitulo = auxTitulo.trim();


				// Obtenci�n fecha de la pelicula	
				String fechaEnCadena = "";
				int fecha = 0;
				boolean tieneFecha = false;

				if( auxTitulo.charAt(auxTitulo.length()-1) == ')'){

					tieneFecha = true;
					fechaEnCadena = auxTitulo.substring(auxTitulo.length()-5, auxTitulo.length()-1 );
					for(int i = 0; i< fechaEnCadena.length() && tieneFecha; i++){
						Character caracterActual = fechaEnCadena.charAt(i);
						if(!Character.isDigit(caracterActual))
							tieneFecha = false;
					}
				}

				//Si la pelicula tiene fecha, se le quita del titulo

				String nombre = auxTitulo;
				if(tieneFecha){
					nombre = auxTitulo.substring(0,auxTitulo.length()-6);
					fecha = Integer.parseInt(fechaEnCadena);
				}

				//Obtenci�n de los generos
				String aux[] = info[info.length-1].split("|");

				ListaEncadenada<String> generos = new ListaEncadenada<String>(); 
				for(int i = 0; i < aux.length; i++)
					generos.agregarElementoFinal(aux[i]);


				//Creaci�n de la pelicula y asignaci�n de sus atributos
				VOPelicula peliculaAAgregar = new VOPelicula();
				peliculaAAgregar.setAgnoPublicacion(fecha);
				peliculaAAgregar.setTitulo(nombre);
				peliculaAAgregar.setGenerosAsociados(generos);

				//A�adiendo las peliculas al arreglo en desorden
				lPeliculas.agregarElementoFinal(peliculaAAgregar);

				linea = lector.readLine();
			}
			
			//Inicializa peliculas
			int num = lPeliculas.darNumeroElementos();
			peliculas = new VOPelicula[num];
			for(int i=0; i<num ;i++){
				peliculas[i]=lPeliculas.darElementoPosicionActual();
				lPeliculas.avanzarSiguientePosicion();
			}

		}
		catch(Exception e){

		}
	}
	public VOPelicula[] arregloNAleatorias(int n){

		
		VOPelicula[] randPeliculas = new VOPelicula[n];

		Random rnd= new Random();
		for(int i=0; i<n; i++){
			int rand = rnd.nextInt(peliculas.length-1);
			randPeliculas[i]=peliculas[rand];
		}

		return randPeliculas;

	}
	
	public VOPelicula[] listaGeneradaDescendetemente(){
		VOPelicula[] listaDescendente = new VOPelicula['z' - 'a'];
		char caracterRequerido = 'z';
		for(int indicePelis= 0, indiceListaDes = 0; indiceListaDes < listaDescendente.length; indicePelis++){
			if(peliculas[indicePelis].getTitulo().toLowerCase().charAt(0) == caracterRequerido){
				listaDescendente[indiceListaDes++] = peliculas[indicePelis];
				caracterRequerido--;
			}
		}
		
		return listaDescendente;
		
	}
	
	public VOPelicula[] listaGeneradaAscendentemente(){
		VOPelicula[] listaAscentemente = new VOPelicula['z' - 'a'];
		Character caracterRequerido = 'a';
		for(int indicePelis= 0, indiceListaAsc = 0; indiceListaAsc < listaAscentemente.length; indicePelis++){
			
			if(peliculas[indicePelis].getTitulo().toLowerCase().charAt(0) == caracterRequerido){
				listaAscentemente[indiceListaAsc++] = peliculas[indicePelis];
				caracterRequerido++;
			}
		}
		return listaAscentemente;
		
	}

	public ListaEncadenada<VOPelicula> listaNAleatoriaa(int n){

		ListaEncadenada<VOPelicula> randListaPeliculas = new ListaEncadenada<>();

		Random rnd= new Random();
		for(int i=0; i<n; i++){
			int rand = (int)(rnd.nextDouble()*peliculas.length);
			randListaPeliculas.agregarElementoFinal(peliculas[rand]);
		}
		return randListaPeliculas;
	}
}
