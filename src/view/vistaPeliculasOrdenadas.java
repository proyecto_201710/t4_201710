package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.ListaEncadenada;
import model.vo.VOPelicula;

public class vistaPeliculasOrdenadas {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		while(!fin){
			printMenu();
			int decision = sc.nextInt();
			
			switch(decision){
			case 1: 
				System.out.println("Ingrese el tamano del que desea la muestra: ");
				int tamanio = sc.nextInt();
				Controller.generarMuestra(tamanio);
				break;
			
			
			case 2:
				VOPelicula[] peliculas = Controller.darArreglo();
				if(peliculas == null || peliculas.length!=0){
					Controller.ordenarMuestraPorNombre();
					System.out.println("El tiempo en ordenar la lista fue de: " + Controller.darTiempo() + " milisegundos.");
				}
				else
					System.out.println("No hay ninguna muestra para ordenar. Debe generar una muestra primero.");

				break;
				
			case 3: 
				ListaEncadenada<VOPelicula> lPeliculas = Controller.darLista();
				if(lPeliculas==null || lPeliculas.darNumeroElementos()!=0){
					Controller.ordenarLMuestraPorNombre();
					System.out.println("El tiempo en ordenar la lista fue de: " + Controller.darTiempo() + " milisegundos.");
				}
				else
					System.out.println("No hay ninguna muestra para ordenar. Debe generar una muestra primero.");

				break;
					
			case 4:
				peliculas = Controller.darArreglo();
				for(VOPelicula peli: peliculas)
					System.out.println(peli.getTitulo());
				break;
				
				
			case 5:
				lPeliculas = Controller.darLista();
				for(VOPelicula peli: lPeliculas)
					System.out.println(peli.getTitulo());
				break;
			case 6:
				fin= true;
				break;
				
			}
			
			
				
			
		}
	}
	private static void printMenu(){
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Generar una muestra de tamano N");
		System.out.println("2. Ordenar por nombre la lista de peliculas");
		System.out.println("3. Ordenar por nombre la lista de peliculas (usando Lista)");
		System.out.println("4. Imprimir el arreglo de peliculas ordenada");
		System.out.println("5. Imprimir la lista de peliculas ordenada");
		System.out.println("6. Salir");
		
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}
}
