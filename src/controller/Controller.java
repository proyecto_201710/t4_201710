package controller;
import java.math.BigDecimal;

import model.data_structures.ListaEncadenada;
import model.logic.*;
import model.vo.VOPelicula;

public class Controller {

	private static MuestroPeliculas muestreoPeliculas = new MuestroPeliculas();
	private static QuickSort<VOPelicula> ordenadorPeliculas = new QuickSort<>();
	private static VOPelicula[] arregloDeMuestras;
	private static ListaEncadenada<VOPelicula> listaDeMuestras = new ListaEncadenada<>();
	
	public static void generarMuestra(int tamanio){
		arregloDeMuestras = muestreoPeliculas.arregloNAleatorias(tamanio);
		listaDeMuestras = muestreoPeliculas.listaNAleatoriaa(tamanio);
	}
	
	public static void ordenarMuestraPorNombre(){
		ordenadorPeliculas.sort(arregloDeMuestras);
	}
	
	public static void ordenarLMuestraPorNombre() {
		ordenadorPeliculas.sort(listaDeMuestras);
		
	}

	public static void generarMuestraAscedente(){
		arregloDeMuestras = muestreoPeliculas.listaGeneradaAscendentemente();
		for(VOPelicula e: arregloDeMuestras)
			listaDeMuestras.agregarElementoFinal(e);
	}
	
	public static void generarMuestraDescedente(){
		arregloDeMuestras = muestreoPeliculas.listaGeneradaDescendetemente();
		for(VOPelicula e: arregloDeMuestras)
			listaDeMuestras.agregarElementoFinal(e);
	}
	
	public static BigDecimal darTiempo(){
		return ordenadorPeliculas.darTiempo();
	}
	
	public static VOPelicula [] darArreglo(){
		return arregloDeMuestras;
	}

	public static ListaEncadenada<VOPelicula> darLista() {
		// TODO Auto-generated method stub
		return listaDeMuestras;
	}
}
