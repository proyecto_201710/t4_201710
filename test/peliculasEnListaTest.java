import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.Iterator;

import controller.*;
import model.*;
import model.data_structures.ListaEncadenada;
import model.vo.VOPelicula;

import org.junit.Test;





import junit.framework.TestCase;

public class peliculasEnListaTest extends TestCase {


	public void setupEscenaario1(){
		Controller.generarMuestraDescedente();
	}

	public void setupEscenario2(){
		Controller.generarMuestraAscedente();
	}

	public void setupEscenario3(){
		Controller.generarMuestra(9000);
	}


	public void testMuestrasSimples(){
		setupEscenaario1();
		Controller.ordenarLMuestraPorNombre();
		ListaEncadenada<VOPelicula> lista = Controller.darLista();
		for(int i = 0; i < lista.darNumeroElementos() - 2; i++)
			if(lista.darElemento(i).compareTo(lista.darElemento(i+1)) > 0){
				System.out.println(i+"\n "+i+1);
				fail("El arreglo no esta en el orden correcto");
				
			}
		

		setupEscenario2();
		Controller.ordenarLMuestraPorNombre();
		lista = Controller.darLista();
		Iterator<VOPelicula> ite = lista.iterator();
		
		VOPelicula ante = ite.next();
		VOPelicula act = ite.next();
		while(ite.hasNext()){
			if(ante.compareTo(act) > 0)
				fail("El arreglo no esta en el orden correcto");
			ante=act;
			act=ite.next();
		}
		

		setupEscenario3();
		Controller.ordenarLMuestraPorNombre();
		lista = Controller.darLista();
		
		Iterator<VOPelicula> ite2 = lista.iterator();
		ante= ite2.next();
		while(ite2.hasNext()){
			act=ite.next();
			if(ante.compareTo(act) > 0)
				fail("El arreglo no esta en el orden correcto");
			ante=act;
		}

	}

	public void testPromedioTamanios(){
		try{
			File archivo = new File("data/tablaL.csv");
			FileWriter escritor = new FileWriter(archivo);
			escritor.write("tamanio, tiempos(ms), promedio" + "\n");

			for(int j = 10; j <= 15; j++){
				for(int i = 0; i< 5; i++){
					Controller.generarMuestra((int)Math.pow(2, j));
					Controller.ordenarLMuestraPorNombre();
					BigDecimal tiempo = Controller.darTiempo();
					escritor.write((int)Math.pow(2, j) + "," + tiempo + ","+ 0 + "\n");
				}

			}

			escritor.close();

		}

		catch(Exception e){

		}
	}

}
