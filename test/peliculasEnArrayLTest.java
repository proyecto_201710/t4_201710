import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;

import controller.*;
import model.*;
import model.vo.VOPelicula;

import org.junit.Test;



import junit.framework.TestCase;

public class peliculasEnArrayLTest extends TestCase {


	public void setupEscenaario1(){
		Controller.generarMuestraAscedente();
	}

	public void setupEscenario2(){
		Controller.generarMuestraDescedente();
	}

	public void setupEscenario3(){
		Controller.generarMuestra(9000);
	}


	public void testMuestrasSimples(){
		setupEscenaario1();
		Controller.ordenarMuestraPorNombre();
		VOPelicula[] lista = Controller.darArreglo();
		for(int i = 0; i < lista.length - 2; i++)
			if(lista[i].compareTo(lista[i+1]) > 0)
				fail("El arreglo no esta en el orden correcto");

		setupEscenario2();
		Controller.ordenarMuestraPorNombre();
		lista = Controller.darArreglo();
		for(int i = 0; i < lista.length - 2; i++)
			if(lista[i].compareTo(lista[i+1]) > 0)
				fail("El arreglo no esta en el orden correcto");

		setupEscenario3();
		Controller.ordenarMuestraPorNombre();
		lista = Controller.darArreglo();
		for(int i = 0; i < lista.length - 2; i++)
			if(lista[i].compareTo(lista[i+1]) > 0)
				fail("El arreglo no esta en el orden correcto");


	}

	public void testPromedioTamanios(){
		try{
			File archivo = new File("data/tabla.csv");
			FileWriter escritor = new FileWriter(archivo);
			escritor.write("tamanio, tiempos(ms), promedio" + "\n");

			for(int j = 15; j <= 20; j++){
				for(int i = 0; i< 5; i++){
					Controller.generarMuestra((int)Math.pow(2, j));
					Controller.ordenarMuestraPorNombre();
					BigDecimal tiempo = Controller.darTiempo();
					escritor.write((int)Math.pow(2, j) + "," + tiempo + ","+ 0 + "\n");
				}

			}

			escritor.close();

		}

		catch(Exception e){

		}
	}

}
